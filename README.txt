Description
===========

Provides token-like macros that accept parameters withing
the node body field or block markup that are replaced by functionality
provided by submodules.


Provided submodules
===================

* Webform macros
	It enables insertion of webform in any node body

* Webform results macros
	It enables insertion of webform *results* in any
  node body (displayed as a table)

* Call to action
  Returns html for call to action button

* Drupal form
  Returns a rendered form

* Get url alias
  Returns the alias of the current node


Usage
=====


Webform
-------
 * In any body field add the [webform ] macro tag.
 *
 * Usage:
 * [webform id [title] [fields] [tplfile] [class] ]
 * Availabile parameters:
 *	- id 	- (mandatory) node id of web form.
 *	- title		- on || off(default)
 *	- fields	- list of fields to show. If not pressent 
 *               show all - use names of fields from the UI
 *	- tplfile	- name of tpl file in the theme or this 
 *               module folder
 *	- class		- classes to add to form wrapper
 * EXAMPLES
 *	- [webform id=2985 title=on fields=name|surname class=class1 tplfile="file.tpl.php"]
 *	- [webform id=2985 fields=name|surname]
 *	- [webform id=2985]

Webform results
---------------
 * In any body field add the [webformresults ] macro tag.
 *
 * Usage:
 * [webformresults id [timestamp] [fields] [timestampname] [tplfile] [class] ]
 * Availabile parameters:
 * 	- id - intiger						//webform nid
 * 	- timestamp - *true/false			//show timestamp
 * 	- fields - field1|field2 OR *NULL	//fields to show, seperated with pipes |, null shows all
 * 	- timestampname - string			//string to show for timestamp name "Timestamp"
 * 	- tplfile - string					//stirng of tpl file to use. if null use default
 *  - class - string OR class1|class2	//classes to be added to wrapper div

Call to action
--------------
 * Returns html for call to action button
 *
 * Usage:
 * [callToAction [bt] [it] [frame] [txt] linktxt url]
 * Availabile parameters:
 * - bt : Type of button -- bt1, bt2
 * - it : Type of icon -- it1, it2, it3
 * - frame : Type of frame -- long, short
 * - txt: Long text
 * - linktxt : Text on the link
 * - url : URL to use on the link

Drupal form
-----------
 * Returns a rendered form
 *
 * Usage:
 * [drupalForm form_id]
 *
 * Availabile parameters:
 * 	- form_id - intiger		//form id

Get url alias
-------------
 * Returns the alias of the current node
 *
 * Usage:
 * [getUrlAlias]


Thanks
======
Developed at D.labs d.o.o (www.dlabs.si)
by:
* Fredi Pevcin <fredi.pevcin@dlabs.si>
* Anže Gros    <anze.gros@dlabs.si>
* Rok Andrée   <rok.andree@dlabs.si>
